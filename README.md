# PyImage3D #

PyImage3D is a Python port of the PHP's Image3D library.

All credit for work done goes to the authors of the original
library, found at:

https://pear.php.net/package/Image_3D/    
https://github.com/pear/Image_3D

All (lack of) credit for porting their hard work
to Python goes to the author of the package.

## Image3D Authors ##

Kore Nordmann <kore@php.net> 
- Designed an implemented Image_3D

Tobias Schlitt <toby@php.net>
- Support for PEAR and application design
- Wrote the ImageMagick output driver

Arne Nordmann <norro@php.net> 
- Wrote the SVGRotate driver and improved the SVG driver.


## PyImage3D Author ##

Primož "Naltharial" Jeras <primoz@naltharial.com>
- Maintaining PyImage3D port

# Port status #

## Working ##

#### [v2.0] ####

Update to Python 3

#### [v1.0] ####

RC

#### [v0.1] ####

- Objects
	1. ZCube
- Drivers
	1. SVG
- Renderers
	1. Perspective
 